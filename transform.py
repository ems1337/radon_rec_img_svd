import cv2 as cv
import numpy as np
import pycuda.autoinit
import pycuda.gpuarray as gpuarray
import skcuda.linalg as linalg


class RadonSVD(object):

    def __init__(self, in_image):
        self.img = in_image
        self.d = int(np.sqrt(in_image.shape[0] ** 2 + in_image.shape[1] ** 2))
        self.dw = int((self.d - in_image.shape[0]) // 2)
        self.dh = int((self.d - in_image.shape[1]) // 2)

    def ForwardRadon(self):
        zoom_img = cv.copyMakeBorder(self.img, self.dh, self.dh, self.dw, self.dw, cv.BORDER_CONSTANT)
        sinogram_ = np.zeros((self.d, 180))
        A = np.zeros((self.d ** 2, self.d * 180), dtype='float32')
        for i in range(180):
            rot = cv.getRotationMatrix2D((self.d // 2, self.d // 2), i, 1.0)
            for s in range(self.d):
                line_ = np.zeros((self.d, self.d), dtype=np.uint8)
                cv.line(line_,  (0, s), (self.d, s), 1)
                img_line = cv.warpAffine(line_, rot, (self.d, self.d))
                A[:, i*self.d + s] = np.float32(img_line.flatten())
            rot_img = cv.warpAffine(zoom_img, rot, (self.d, self.d))
            sinogram_[:, i] = np.sum(rot_img, axis=0)
        cv.normalize(sinogram_, sinogram_, 0, 1, cv.NORM_MINMAX)
        return sinogram_, A

    def RecImgSVD(self, sinogram, A):
        sinogram_col = sinogram.T.flatten()
        linalg.init()
        a_gpu = gpuarray.to_gpu(A)
        U, s, VT = linalg.svd(a_gpu)
        d = np.zeros(self.d ** 2)
        for i in range(self.d ** 2):
            d[i] = 0 if s.get()[i] < 0.008 else 1. / s.get()[i]
        D = np.zeros(A.shape, dtype='float32')
        D[:A.shape[1], :A.shape[0]] = np.diag(d)
        B = VT.get().T.dot(D.T).dot(U.get().T)
        x = np.abs(sinogram_col.dot(B))
        rec_img = np.zeros((self.d, self.d), dtype=np.uint8)
        cv.normalize(x, x, 0, 255, cv.NORM_MINMAX)
        for i in range(self.d):
            for j in range(self.d):
                rec_img[j][i] = x[i*self.d + j]
        return rec_img


if __name__ == '__main__':
    img = cv.imread('../ddd/1616.png', cv.IMREAD_GRAYSCALE)
    radon = RadonSVD(img)
    sinogram, A = radon.ForwardRadon()
    rec_img = radon.RecImgSVD(sinogram, A)
    cv.imshow('image', img)
    cv.imshow('sinogram', sinogram)
    cv.imshow('rec_image', rec_img)
    cv.waitKey(10000)
